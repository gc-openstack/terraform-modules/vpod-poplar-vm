output "id" {
  value = openstack_compute_instance_v2.poplar.id
}

output "ctrl_port_id" {
  value = openstack_networking_port_v2.ctrl.id
}

output "poplar_storage_ip" {
  value = element(openstack_networking_port_v2.storage.all_fixed_ips, 0)
}