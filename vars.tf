#
# Required
#
variable "name_suffix" {
  type = string
}

variable "vpod_name" {
  type = string
}

variable "ctrl_network_id" {
  type = string
}

variable "ctrl_subnet_id" {
  type = string
}

variable "rnic_network_id" {
  type = string
}

variable "rnic_subnet_id" {
  type = string
}

variable "storage_network_id" {
  type = string
}

variable "storage_subnet_id" {
  type = string
}

variable "vpod_floatingip" {
  type = string 
  default = ""
}

variable "poplaraz" {
  type = string
}

variable "vpod_common_sg_id" {
  type = string
}

#
# Optional
#
variable "dns_base" {
  type    = string
  default = "openstack.example.net"
}

variable "flavor" {
  type    = string
}

variable "image" {
  type    = string
}

variable "key_pair" {
  type    = string
  default = "seed"
}

variable "cidr_hint" {
}

variable "docker_vol_size" {
  type = number
  default = "50"
}