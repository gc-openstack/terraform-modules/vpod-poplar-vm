Terraform Module: vpod-poplar-vm
================================

Overview
--------

This module is used to create a virtual instance to operate as a user-facing Poplar host instance.

The default size of a virtual Poplar instance will be an r6525.full (ie a full R6525 VM), but this can be sized down for smaller vPODs.   The Poplar VM will be placed into a specific Openstack AZ, which will direct it to a physical host in the correct LR. 

This module will also create a Cinder volume that will be attached to this instance.   The volume is formatted as LVM and mounted at /var/lib/docker (configured via cloud-init)

Networking
----------
The vpod-poplar instance is connected to:
* Dedicated vPOD control network (10.3.0.0/24)
* Dedicated vPOD RNIC network (10.5.0.0/16)
* Dedicated vPOD storage network (10.12.X.0/24)

Prerequisites
-------------
* This module assumes that an NFS filesystem /public1 is available on 10.12.{cidr_hint}.250.   This is added to /etc/fstab, mounting to /mnt/public

Variables
=========
Required
--------
* name_suffix - the VM will be created with the name 'vpod<vpod name>-poplar-<name_suffix>'
* vpod_name - the name of the vPOD
* ctrl_network_id - the ID of the control network to attach to (sourced from vpod-nets module)
* ctrl_subnet_id - the ID the control network subnet to attach to (sourced from vpod-nets module)
* rnic_network_id - the ID of the RNIC network to attach to (sourced from vpod-nets module)
* rnic_subnet_id - the ID the RNIC network subnet to attach to (sourced from vpod-nets module)
* storage_network_id - the ID of the storage network to attach to (sourced from vpod-nets module)
* storage_subnet_id - the ID the storage network subnet to attach to (sourced from vpod-nets module)
* poplaraz - specifies which AZ (and therefore, which LR) the Poplar VM will be created.
* cidr_hint - number to be used for the third octet of each subnet
* vpod_common_sg_id  - the ID of a common security group for all Poplar hosts in a vPOD


Optional
--------
* vpod_floatingip - the floating IP to be attached to the Poplar instance.   If omitted, a random IP will be assigned from the relevant pool.
* dns_base - the domain to be used for the interfaces (default: openstack.example.net)
* flavor - the size of the instance (default: r6525.full)
* image - the image the instance should be booted from
* key_pair - the name of the key pair to be injected into the default user.

Usage
=====
This module should be used once per Poplar instance required.

    module "vpod-poplar-1" {
    source             = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-poplar-vm-module.git"
    name_suffix        = "1"
    vpod_name          = var.vpod_name
    ctrl_network_id    = module.networks.ctrl_network_id
    ctrl_subnet_id     = module.networks.ctrl_subnet_id
    rnic_network_id    = module.networks.rnic_network_id
    rnic_subnet_id     = module.networks.rnic_subnet_id
    storage_network_id = module.networks.storage_network_id
    storage_subnet_id  = module.networks.storage_subnet_id
    vpod_common_sg_id  = module.networks.vpod_internal_sg 
    vpod_floatingip    = var.vpod_floatingip
    poplaraz           = var.poplaraz
    flavor             = var.flavor
    cidr_hint          = var.cidr_hint
    image              = var.image
    }

with example tfvars:

    vpod_name         = "vpod7"
    ctrl_vlan         = 1234
    rnic_vlan         = 2345
    storage_vlan      = 3456
    cidr_hint         = 100
    ipum_csv          = "rnic_rack8.csv"
    vpod_floatingip   = "1.2.3.4"
    poplaraz          = "rack8-AZ"
    assigned_ipums    = ["rack8-ipum1", "rack8-ipum2", "rack8-ipum3", "rack8-ipum4", "rack8-ipum5", "rack8-ipum6", "rack8-ipum7", "rack8-ipum8", "rack8-ipum9", "rack8-ipum10", "rack8-ipum11", "rack8-ipum12", "rack8-ipum13", "rack8-ipum14", "rack8-ipum15", "rack8-ipum16" ]
    flavor            = "r6525.full"

Cloud Init Actions
------------------
A cloud-init config file is templated to user-data, and handed to instance at creation.   This performs the following actions:
* Addition of NFS filesystem mounts to /etc/fstab
* Addition of /var/lib/docker to /etc/fstab
* Installation of useful packages
* Templating of systemd-networkd files to disable DNS servers provided via DHCP, leaving DNS available only on 10.3.X.0/24.   This will no longer be required following planned DNS enhancements.
* Restart of systemd-networkd, following the above action.  
* Creation of LVM configuration for /var/lib/docker

Development and Feedback
========================
The development of this tooling is ongoing, and any feedback is appreciated.  If you find any errors or bugs, please create an issue on the appropriate repository, or contact your Graphcore representative.