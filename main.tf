terraform {
  required_version = ">= 1.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.35"
    }
  }
}

locals {
  name = "${var.vpod_name}-poplar-${var.name_suffix}"
}

resource "openstack_networking_secgroup_v2" "pop" {
  name        = local.name
  description = "Security group for vipu controller"
}

# Expose ssh via all IPv4 addresses
resource "openstack_networking_secgroup_rule_v2" "ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.pop.id
}

resource "openstack_networking_secgroup_rule_v2" "node_exporter" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix = "10.3.${var.cidr_hint}.0/24"
  security_group_id = openstack_networking_secgroup_v2.pop.id
}

resource "openstack_networking_port_v2" "ctrl" {
  name               = local.name
  network_id         = var.ctrl_network_id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.pop.id, var.vpod_common_sg_id ]
  fixed_ip {
    subnet_id = var.ctrl_subnet_id
  }
}

variable "rnic_profile" {
    type = map
    description = "Custom binding information, as terraform map"
    default = {
        capabilities = ["switchdev"]
    }
}

resource "openstack_networking_port_v2" "rnic" {
  name           = "${local.name}-rnic"
  network_id     = var.rnic_network_id
  admin_state_up = "true"
  binding {
    vnic_type = "direct"
    profile = jsonencode(var.rnic_profile)
  }
  fixed_ip {
    subnet_id = var.rnic_subnet_id
  }
  port_security_enabled = "false"
  # don't overrite os-vif adding chosen PCI device
  lifecycle {
    ignore_changes = [
      binding,
    ]
  }
}

resource "openstack_networking_port_v2" "storage" {
  name           = "${local.name}-storage"
  network_id     = var.storage_network_id
  admin_state_up = "true"
  binding {
    vnic_type = "direct"
    profile = jsonencode(var.rnic_profile)
  }
  fixed_ip {
    subnet_id = var.storage_subnet_id
  }
  port_security_enabled = "false"
  # don't overrite os-vif adding chosen PCI device
  lifecycle {
    ignore_changes = [
      binding,
    ]
  }
}

data "openstack_images_image_v2" "image" {
  name = var.image
}
data "openstack_compute_flavor_v2" "flavor" {
  name = var.flavor
}

data "template_file" "cloud-config-template" {
  template = file("${path.module}/templates/cloud-config.tpl")
  vars = {
    rnic_mac = openstack_networking_port_v2.rnic.mac_address
    storage_mac = openstack_networking_port_v2.storage.mac_address
    cidr_hint = var.cidr_hint
  }
}
resource "openstack_compute_instance_v2" "poplar" {
  name            = local.name
  image_id        = data.openstack_images_image_v2.image.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor.id
  key_pair        = var.key_pair
  config_drive    = true
  availability_zone = var.poplaraz
  user_data = data.template_file.cloud-config-template.rendered
  lifecycle {
    prevent_destroy = true
  }
  network {
    port = openstack_networking_port_v2.ctrl.id
  }
  network {
    port = openstack_networking_port_v2.rnic.id
  }
  network {
    port = openstack_networking_port_v2.storage.id
  }
}

resource "openstack_blockstorage_volume_v3" "poplar_docker" {
  name            = "${local.name}-docker-vol"
  size            = var.docker_vol_size
}

resource "openstack_compute_volume_attach_v2" "pop_docker_attach" {
  instance_id     = openstack_compute_instance_v2.poplar.id
  volume_id       = openstack_blockstorage_volume_v3.poplar_docker.id
}
