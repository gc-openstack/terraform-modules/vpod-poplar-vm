#cloud-config

mounts:
 - [ ephemeral0, /localdata, auto, "defaults" ]
 - [ /dev/mapper/data-docker, /var/lib/docker, auto, "defaults" ]
 - [ "10.12.${cidr_hint}.250:/public1", "/mnt/public", "nfs", "ro,relatime,vers=3,hard,proto=tcp,nconnect=16,timeo=600,retrans=2,sec=sys,mountvers=3,mountport=2049,mountproto=udp,rsize=524288,wsize=524288,namlen=255"]

packages:
 - nfs-common
 - fio

write_files:
- content: |
    [Match]
    MACAddress=${rnic_mac}

    [Network]
    DHCP=yes

    [DHCPv4]
    UseDNS=false
  path: /etc/systemd/network/06-rnic.network
- content: |
    [Match]
    MACAddress=${storage_mac}

    [Network]
    DHCP=yes

    [DHCPv4]
    UseDNS=false
  path: /etc/systemd/network/07-storage.network
runcmd:
  - [ vgcreate, data, "/dev/vdc" ]
  - [ lvcreate, -l, "100%FREE",  -n, docker, data]
  - [ mkfs.xfs, "/dev/mapper/data-docker"]
  - [ mkdir, "/var/lib/dockermv"]
  - [ systemctl, stop, docker ]
  - [ mv, "/var/lib/docker/*", "/var/lib/dockermv"]
  - [ mkdir, "/var/lib/docker"]
  - [ mount, "/var/lib/docker"]
  - [ mv, "/var/lib/dockermv/*", "/var/lib/docker"]
  - [ systemctl, start, docker ]
  - [ rmdir, "/var/lib/dockermv"]
  - [ systemctl, restart, systemd-networkd]